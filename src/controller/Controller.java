package controller;

import api.ITaxiTripsManager;
import model.data_structures.Lista;
import model.data_structures.digraph.Camino;
import model.data_structures.digraph.Componente;
import model.logic.TaxiTripsManager;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(double distanciaReferencia)
	{
		return manager.cargarSistema(distanciaReferencia);
	}
	
	public static String verticeCongestionado()
	{
		return manager.verticeCongestionado();
	}
	
	public static Lista<Componente> req2() throws Exception
	{
		return manager.req2();
	}
	
	public static String req3() throws Exception
	{
		return manager.req3();
	}
	
	public static Camino req4() throws Exception
	{
		return manager.req4();
	}

	public static Lista<Camino> req5() throws Exception
	{
		return manager.req5();
	}
	
	public static Lista<Camino> req6() throws Exception
	{
		return manager.req6();
	}
	
	
}
