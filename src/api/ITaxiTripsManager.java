package api;

import model.data_structures.Lista;
import model.data_structures.digraph.Camino;
import model.data_structures.digraph.Componente;

/**
 * API para la clase de logica principal  
 */

public interface ITaxiTripsManager 
{
	
	//1C
	/**
	 * Dada la direccion del json que se desea cargar, se generan vo's, estructuras y datos necesarias
	 * @param direccionJson, ubicacion del json a cargar
	 * @param distancia, distancia a partir de la cual se crea el grafo.
	 * @return true si se lo logro cargar, false de lo contrario
	 */
	public boolean cargarSistema(double distancia);
	
	public String verticeCongestionado();
	
	public Lista<Componente> req2() throws Exception;
	
	public String req3();
	
	public Camino req4() throws Exception;
	
	public Lista<Camino> req5() throws Exception;
	
	public Lista<Camino> req6(); 

}