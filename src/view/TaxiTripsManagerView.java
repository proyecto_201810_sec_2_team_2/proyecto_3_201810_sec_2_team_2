package view;


import java.util.ArrayList;
import java.util.Scanner;


import controller.Controller;
import model.data_structures.Lista;
import model.data_structures.digraph.Camino;
import model.data_structures.digraph.Componente;
import model.data_structures.digraph.Editor;
import model.logic.TaxiTripsManager;
import model.vo.Service;
import model.vo.Taxi;
import model.vo.VerticeServicios;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) throws Exception 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			
			case 1: // cargar informacion a procesar

				System.out.println("Se carga el grafo del archivo large con una distancia radial de 100 metros.");
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(100);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

					
			case 2:
				
				String rta2 = Controller.verticeCongestionado();
				
				System.out.println(rta2);
				String[] r = rta2.split("/");
				
		
				Editor d= new Editor();
				
					Editor.dibujoRequerimiento1(Double.parseDouble(r[0]), Double.parseDouble(r[1]));

				break;
				
			case 3: 
				Lista<Componente> rta3 = Controller.req2();
				for (int i = 0 ; i <rta3.darLongitud(); i++)
				{
					System.out.println("Componente " + (i+1));
					System.out.println( "\tNumero de vertices: "+rta3.darElemento(i).numVertice);
					System.out.println("\tColor: " + rta3.darElemento(i).color);
				}
				break;
				
			case 4: 
				
				String rta4 = Controller.req3();
				Editor d4= new Editor();
				d4.dibujoRequerimiento3(rta4);
				
				break;
			case 5: 
				
				Camino rta5 = Controller.req4();
				ArrayList<String> lista5 = new ArrayList<String>();
//			System.out.println("size rta5 " + rta5.darVertices2().size());
				for(int i5 = 0; i5<rta5.darVertices2().size(); i5++)
				{
					VerticeServicios v5 = (VerticeServicios) rta5.darVertices2().get(i5);
					lista5.add(v5.darId());
				}
				Editor d5= new Editor();
				
				if(rta5.darVertices2().size()>1){
				
				System.out.println("d5 " + rta5.darVertices2().size());
					d5.dibujoRequerimiento4(lista5);
				}

				break;
				
			case 6: 
				Lista<Camino> rta6 = Controller.req5();
				break;
				
			case 7: 

				Lista<Camino> rta7 = Controller.req6();
				break;
				
			case 8: 
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 3----------------------");
		System.out.println("1. Cargar el grafo del archivo large con distancia radial de 100 metros.");
		System.out.println("");
		System.out.println("---------------------Requerimientos----------------------");
		System.out.println("2. Mostrar el v�rtice m�s congestionado.");
		System.out.println("3. Calcular los componentes fuertemente conexos presentes en el grafo.");
		System.out.println("4. Requerimiento Google Maps.");
		System.out.println("5. Encontrar el	camino de costo	m�nimo (menor distancia) para un servicio que inicia en un punto (latitud, longitud) escogido aleatoriamente de la informaci�n cargada del archivo de calles (StreetLines.csv) y finaliza en un	 punto (latitud, longitud)	escogido tambi�n de manera	aleatoria del archivo de calles.");
		System.out.println("6. Dado un servicio entre dos puntos del archivo de calles encontrar los caminos de mayor y menor duraci�n entre dichos puntos.");
		System.out.println("7. Dado un servicio entre dos puntos del archivo de calles determinar si existe un camino en e que no se deba pagar peaje. ");
		System.out.println("8. Fin ");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- �Qu� fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Ingrese el n�mero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
	
	private static void printMenuDistancia()
	{
		System.out.println("-- �Qu� distancia radial de referencia usar para la creaci�n del grafo?");
		System.out.println("-- 1. 25 metros");
		System.out.println("-- 2. 50 metros");
		System.out.println("-- 3. 70 metros");
		System.out.println("-- 4. 100 metros");
		System.out.println("-- Ingrese el n�mero correspondiente a la distancia radial de referencia para crear el grafo y presione <Enter> para confirmar: (e.g., 1)");
	}

	private static void printMenuJSON()
	{
		System.out.println("-- �Desea cargar los datos del JSON?");
		System.out.println("-- 1. Si");
		System.out.println("-- 2. No");
		System.out.println("-- Ingrese el numero de la opci�n para la carga de datos y presione <Enter> para confirmar: (e.g., 1)");
	}
}
