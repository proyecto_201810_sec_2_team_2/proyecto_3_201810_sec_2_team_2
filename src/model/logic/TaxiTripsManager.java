package model.logic;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.Lista;
import model.data_structures.digraph.Arco;
import model.data_structures.digraph.Camino;
import model.data_structures.digraph.CaminosMinimos;
import model.data_structures.digraph.Componente;
import model.data_structures.digraph.GrafoDirigido;
import model.data_structures.digraph.IVertice;
import model.data_structures.digraph.Iterador;
import model.data_structures.digraph.Kosaraju;
import model.data_structures.digraph.Vertice;
import model.vo.ArcoServicios;
import model.vo.ControladorMundo;
import model.vo.ControladorMundo2;
import model.vo.Service;
import model.vo.VerticeServicios;

public class TaxiTripsManager implements ITaxiTripsManager
{
	private ControladorMundo controladorMundo;
	private ControladorMundo2 controladorMundo2;
	private double distancia; 
	private double size;
	private ArrayList<String[]> csv;
	private ArrayList<String> latlong;


	//Carga el grafo del archivo large :)
	@Override 
	public boolean cargarSistema(double distanciaRef)
	{
		try {
			size = 437160;
			controladorMundo = new ControladorMundo(distanciaRef);
			controladorMundo2 = new ControladorMundo2(distanciaRef);
			csv = new ArrayList<>();
			latlong = new ArrayList<>();

			if(cargarVertices()&&cargarArcos()){
				controladorMundo.imprimirInfo();
				controladorMundo2.imprimirInfo();
				cargarArchivoCalles();
			}
			else return false;

		}
		catch(Exception e) {
			e.printStackTrace();
		}	
		return false;
	}

	public boolean cargarArchivoCalles() throws Exception
	{
		boolean rta = false;
		String archivo = "./data/ChicagoStreets.csv";
		BufferedReader br = null;
		String linea = "";
		String separador = ";";
		int i = 0;
		try {
			br = new BufferedReader(new FileReader(archivo));
			while((linea = br.readLine())!=null){
				String[] datos = linea.split(separador);
				//Imprimir datos
				for (int j = 0; j < 7; j++){
					//				{System.out.println(j + " " + datos[j]);}

					i++;
				}
				csv.add(datos);
				latlong.add(datos[6]);
				rta = true;
				//				System.out.println(csv.size());
				//				System.out.println(latlong.size()+"latlong");
			}
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		finally 
		{
			if(br!= null)
			{
				try
				{
					br.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return rta;
	}


	public boolean cargarArcos() throws Exception 
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json

		InputStream inputStream;
		try 
		{
			inputStream = new FileInputStream("./data/grafos/grafo-LARGE-100.0m/arcos-LARGE-100.0m.json");
		} catch (FileNotFoundException e1) {
			System.out.println("La persistencia de los arcos que desea cargar no existe.");
			return false;
		}

		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		try
		{
			while (reader.hasNext())
			{

				// Instancia un servicio del json
				ArcoServicios nArco = gson.fromJson(reader, ArcoServicios.class);

				controladorMundo.agregarArco(nArco);
				controladorMundo2.agregarArco(nArco);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		reader.close();
		return true;
	}

	public boolean cargarVertices() throws Exception
	{
		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json
		InputStream inputStream;
		try
		{
			inputStream = new FileInputStream("./data/grafos/grafo-LARGE-100.0m/vertices-LARGE-100.0m.json");
		} 
		catch (FileNotFoundException e1) 	
		{
			System.out.println("La persistencia de los vertices que desea cargar no existe.");
			return false;
		}
		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
		Gson gson = new GsonBuilder().create();

		// Esto recorre todos los elementos que estan en el json. Como no los guarda es mas memory friendly
		reader.beginArray();
		try
		{
			while (reader.hasNext())
			{
				// Instancia un servicio del json
				VerticeServicios nVertice = gson.fromJson(reader, VerticeServicios.class);

				controladorMundo.agregarVertice(nVertice);
				controladorMundo2.agregarVertice(nVertice);

			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		reader.close();
		return true;
	}




	//REQUERIMIENTOS FUNCIONALES -------------------------------------------------------------------------------

	public String verticeCongestionado()
	{
		//		String str="";
		//		GrafoDirigido gf= controladorMundo.darGrafo();
		//		List<VerticeServicios> vertices=gf.darVertices();
		//		int mayor=0;
		//		VerticeServicios vMayor=null;
		//		for(VerticeServicios v: vertices)
		//		{
		//			int total= v.darNumSucesores()+v.darNumPredecesores();
		//			if(total>mayor)
		//			{
		//				mayor= total;
		//				vMayor=v;
		//			}
		//		}
		//		if(vMayor!=null)
		//		{
		//			str+= "Latitud: "+ vMayor.darLatitud() +"\n"+
		//				 "Longitud: "+ vMayor.darLongitud()+ "\n"+
		//				"Predecesores: " +vMayor.darNumPredecesores() +"\n" + 
		//				"Sucesores: " + vMayor.darNumSucesores();
		//		}
		//	
		//		
		//		return str;

		String rta="";
		GrafoDirigido gf= controladorMundo.darGrafo();
		HashMap mapa = gf.darMap();	
		int mayor=0;
		String id = "";
		int pre = 0;
		int pos = 0;
		Vertice ver = null;

		Iterator it = mapa.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry pair = (Map.Entry) it.next();
			//			System.out.println(pair.getKey() + " " + pair.getValue());
			Vertice vertice = (Vertice) pair.getValue();
			int total = vertice.darNumeroPredecesores()+vertice.darNumeroSucesores();
			//			System.out.println("total1 " + total);
			if(total>mayor)
			{
				id = (String) pair.getKey();
				mayor = total;
				pre = vertice.darNumeroPredecesores()*336;
				pos = vertice.darNumeroSucesores()*193;
				//				System.out.println(id +" Llegan " +  pre+ " servicios. Salen " + pos+ " servicios.");
			}
		}

		System.out.println("Total de servicios: " + (pre+pos) + ", salen " + pos + " y llegan " +pre+ ".");
		return id;
	}

	//	public String vertice2()
	//	{
	//		String rta="";
	//		GrafoDirigido gf= controladorMundo.darGrafo();
	//		HashMap mapa = gf.darMap();	
	//
	//		Iterator it = mapa.entrySet().iterator();
	//		while(it.hasNext()){
	//			Map.Entry pair = (Map.Entry) it.next();
	//			System.out.println(pair.getKey() + " " + pair.getValue());
	//			Vertice vertice = (Vertice) pair.getValue();
	//			System.out.println("sucesores " + vertice.darNumeroSucesores());
	//		}
	//
	//		return rta;
	//	}

	public Lista<Componente> req2() throws Exception
	{
		Lista<Componente> rta = new Lista<Componente>();
		rta = controladorMundo.darGrafo().contarComponentesConexos();
		Kosaraju k = new Kosaraju(controladorMundo.darGrafo(), controladorMundo2.darGrafo());
		System.out.println(k.count());
		System.out.println(rta.darLongitud());
		return rta;
	}

	public String req3() {
		// TODO Auto-generated method stub
		GrafoDirigido gf = controladorMundo.darGrafo();
		List<VerticeServicios> rta  = gf.darVertices();
		int total = 0;
		String ret="";
		for(VerticeServicios v: rta)
		{
			//System.out.println(v.darIdServicios().size());
			System.out.println("Vertice " + v.darId() + " tiene una densidad de " + (v.darIdServicios().size()/size) + " y tendr�a un radio de " + (v.darIdServicios().size()/size)*100 + " metros.");
			ret+= v.darId()+":::"+(v.darIdServicios().size()/size)+":::"+(v.darIdServicios().size()/size)*300+";;;";
			total += v.darIdServicios().size();
		}
		return ret;
	}

	public Camino req4() throws Exception
	{
		Camino rta = null;
		double tiempo = 0;
		double distancia = 0;
		double valor = 0;
		GrafoDirigido gf= controladorMundo.darGrafo();

		Random random = new Random();
		int numero1 = random.nextInt(csv.size());

		Random random2 = new Random();
		int numero2 = random2.nextInt(csv.size());

		String punto1 = latlong.get(numero1);
		String[] punto11 = punto1.split(" ");
		System.out.println("lat1 " + punto11[1]);
		System.out.println("lon1 " + punto11[0]);

		System.out.println(" ");

		String punto2 = latlong.get(numero2);
		String[] punto21 = punto2.split(" ");
		System.out.println("lat2 " + punto21[1]);
		System.out.println("lon2 " + punto21[0]);
		System.out.println("");

		VerticeServicios ver1 = controladorMundo.darVertice(Double.parseDouble(punto11[1]), Double.parseDouble(punto11[0]));
		System.out.println("vertice1 " + ver1.darId());
		Vertice origen = (Vertice) gf.darMap().get(ver1.darId());
		VerticeServicios origenv = (VerticeServicios) origen.darInfoVertice();
		rta = new Camino(origen);
		rta.addVertice2(origenv);
		System.out.println("camino " + rta.darVertices().size());
		
		VerticeServicios ver2 = controladorMundo.darVertice(Double.parseDouble(punto21[1]), Double.parseDouble(punto21[0]));
		System.out.println("vertice2 " + ver2.darId());
		System.out.println("");

		Boolean existe = gf.hayCamino(ver1.darId(), ver2.darId());
		if(!existe)
		{
			System.out.println("No existe un camino entre los vertices.");
		}
		else
		{
			//			System.out.println(existe);
			CaminosMinimos dij = gf.dijkstra(ver1.darId());
			Vertice v2 = (Vertice) gf.darMap().get(ver2.darId());

			Iterador iter = dij.darCaminoMinimo(v2);
			Iterador iter2 = dij.darCaminoMinimo(v2);
			int k = 0;
			int k2 = 0;
			Vertice  actual = null;		
			
			while(iter.haySiguiente())
			{
				if (k==0){
					Vertice orig = (Vertice) iter.darSiguiente();
					System.out.println(orig.darId());
					Vertice orig2 = (Vertice) gf.darMap().get(orig);
					VerticeServicios orig3 = (VerticeServicios) origen.darInfoVertice();
//					System.out.println("longitud " + orig3.darLongitud());
					rta.addVertice2(orig3);
					
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					Vertice fin2 = (Vertice) gf.darMap().get(fin);
					VerticeServicios fin3 = (VerticeServicios) origen.darInfoVertice();

					rta.addVertice2(fin3);
					System.out.println(fin.darId());
					
					ArcoServicios arco = (ArcoServicios) gf.darArco(orig.darId(), fin.darId());
					tiempo+=arco.darPromedioTiempo();
					distancia+=arco.darPromedioDistancia();
					valor += arco.darPromedioValor();
				}
				
				else
				{
					Vertice orig = actual;
					Vertice fin = (Vertice) iter.darSiguiente();
					actual = fin;
					
					ArcoServicios arco = (ArcoServicios) gf.darArco(orig.darId(), fin.darId());
					
					tiempo+=arco.darPromedioTiempo();
					distancia+=arco.darPromedioDistancia();
					valor += arco.darPromedioValor();
					System.out.println(fin.darId());
					Vertice fin2 = (Vertice) gf.darMap().get(fin);
					VerticeServicios fin3 = (VerticeServicios) origen.darInfoVertice();

					rta.addVertice2(fin3);
				}
				
				k++;
			}

			System.out.println("Tiempo estimado " + tiempo);
			System.out.println("Distancia estimada " + distancia);
			System.out.println("Valor estimado " + valor);

		}
//		System.out.println("rta " + rta.darLongitud());
		return rta;

	}


	@Override
	public Lista<Camino> req5() throws Exception {
		// TODO Auto-generated method stub
		
		Camino rta = null;
		double tiempo = 0;
		double distancia = 0;
		double valor = 0;
		GrafoDirigido gf= controladorMundo.darGrafo();

		Random random = new Random();
		int numero1 = random.nextInt(csv.size());

		Random random2 = new Random();
		int numero2 = random2.nextInt(csv.size());

		String punto1 = latlong.get(numero1);
		String[] punto11 = punto1.split(" ");
		//		System.out.println(punto1);
		//		String punto11 = punto1.replace(" ", "/");
		System.out.println("lat1 " + punto11[1]);
		System.out.println("lon1 " + punto11[0]);

		System.out.println(" ");

		String punto2 = latlong.get(numero2);
		String[] punto21 = punto2.split(" ");
		//		System.out.println(punto1);
		//		String punto11 = punto1.replace(" ", "/");
		System.out.println("lat2 " + punto21[1]);
		System.out.println("lon2 " + punto21[0]);
		System.out.println("");

		VerticeServicios ver1 = controladorMundo.darVertice(Double.parseDouble(punto11[1]), Double.parseDouble(punto11[0]));
		System.out.println("vertice1 " + ver1.darId());
		Vertice origen = (Vertice) gf.darMap().get(ver1.darId());
		VerticeServicios origenv = (VerticeServicios) origen.darInfoVertice();
		rta = new Camino(origen);
		rta.addVertice2(origenv);
		System.out.println("camino " + rta.darVertices().size());
		
		VerticeServicios ver2 = controladorMundo.darVertice(Double.parseDouble(punto21[1]), Double.parseDouble(punto21[0]));
		System.out.println("vertice2 " + ver2.darId());
		System.out.println("");

		Boolean existe = gf.hayCamino(ver1.darId(), ver2.darId());
		if(!existe)
		{
			System.out.println("No existe un camino entre los vertices.");
		}
		return null;
		
	}

	@Override
	public Lista<Camino> req6() {
		// TODO Auto-generated method stub
		return null;
	}
}