package model.data_structures.digraph;

/*  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class DirectedDFS {
	private boolean[] marked;  // marked[v] = true if v is reachable from source(s)
	private int count;         // number of vertices reachable from source(s)

	/**
	 * Computes the vertices in digraph {@code G} that are
	 * reachable from the source vertex {@code s}.
	 * @param G the digraph
	 * @param s the source vertex
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public DirectedDFS(GrafoDirigido G, Vertice s) {
//		marked = new boolean[G.V()];
//		validateVertex(s);
		dfs(G, s);
	}

	/**
	 * Computes the vertices in digraph {@code G} that are
	 * connected to any of the source vertices {@code sources}.
	 * @param G the graph
	 * @param sources the source vertices
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 *         for each vertex {@code s} in {@code sources}
	 */
	public DirectedDFS(GrafoDirigido G, Iterable<Vertice> sources) {
//		marked = new boolean[G.V()];
		validateVertices(sources);
		for (Vertice v : sources) {
			if (!v.marcadoDFS2()) dfs(G, v);
		}
	}

	private void dfs(GrafoDirigido G, Vertice v) { 
		count++;
		v.marcarDFS2();
//		for (int w : G.adj(v)) {
//			if (!marked[w]) dfs(G, w);
//		}
		
		for (int i = 0; i<G.darAdyacentes(v).size(); i++)
	       {
	    	   Vertice actual = (Vertice) G.darAdyacentes(v).getByPos(i);
	    	   if(!actual.marcadoDFS2())
	    	   {
	    		   dfs(G,v);
	    	   }
	       }
	}

	/**
	 * Is there a directed path from the source vertex (or any
	 * of the source vertices) and vertex {@code v}?
	 * @param  v the vertex
	 * @return {@code true} if there is a directed path, {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean marked(Vertice v) {
//		validateVertex(v);
//		return marked[v];
		return v.marcado();
	}

	/**
	 * Returns the number of vertices reachable from the source vertex
	 * (or source vertices).
	 * @return the number of vertices reachable from the source vertex
	 *   (or source vertices)
	 */
	public int count() {
		return count;
	}

//	// throw an IllegalArgumentException unless {@code 0 <= v < V}
//	private void validateVertex(int v) {
//		int V = marked.length;
//		if (v < 0 || v >= V)
//			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
//	}

	// throw an IllegalArgumentException unless {@code 0 <= v < V}
	private void validateVertices(Iterable<Vertice> vertices) {
		if (vertices == null) {
			throw new IllegalArgumentException("argument is null");
		}
//		int V = marked.length;
//		for (Vertice v : vertices) {
//			if (v < 0 || v >= V) {
//				throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
//			}
//		}
	}

}