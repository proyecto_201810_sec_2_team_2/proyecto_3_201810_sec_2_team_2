package model.data_structures.digraph;

import model.data_structures.SeparateChainingHashTable;

/*  @author Robert Sedgewick
*  @author Kevin Wayne
*/
public class TransitiveClosure {
   private DirectedDFS[] tc;  // tc[v] = reachable from v
   private SeparateChainingHashTable<String, DirectedDFS> hash;

   /**
    * Computes the transitive closure of the digraph {@code G}.
    * @param G the digraph
 * @throws Exception 
    */
   public TransitiveClosure(GrafoDirigido G) throws Exception {
       tc = new DirectedDFS[G.darVertices().size()];
       hash = new SeparateChainingHashTable<String, DirectedDFS>(G.darVertices().size());
       for (int i = 0; i < G.darVertices().size(); i++)
       {
    	   Vertice v = (Vertice) G.darVertices().get(i);
//           tc[v] = new DirectedDFS(G, v);
    	   hash.put((String)v.darId(), new DirectedDFS(G,v));
    	   
       }
   }

   /**
    * Is there a directed path from vertex {@code v} to vertex {@code w} in the digraph?
    * @param  v the source vertex
    * @param  w the target vertex
    * @return {@code true} if there is a directed path from {@code v} to {@code w},
    *         {@code false} otherwise
 * @throws Exception 
    * @throws IllegalArgumentException unless {@code 0 <= v < V}
    * @throws IllegalArgumentException unless {@code 0 <= w < V}
    */
   public boolean reachable(Vertice v, Vertice w) throws Exception {
//       validateVertex(v);
//       validateVertex(w);
       return hash.get((String) v.darId()).marked(w);
   }

   // throw an IllegalArgumentException unless {@code 0 <= v < V}
   private void validateVertex(int v) {
       int V = tc.length;
       if (v < 0 || v >= V)
           throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
   }

   
   

}
