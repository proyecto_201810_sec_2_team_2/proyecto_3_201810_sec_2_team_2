 package model.data_structures.digraph;

import java.awt.Color;
import java.util.Random;

public class Componente {
	
	public Color color;
	
	public int numVertice;
	
	public Componente (int pNum)
	{
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		color = new Color(r, g, b);
		
		numVertice = pNum;
	}
	
	public void addVer()
	{
		numVertice++;
	}

}
