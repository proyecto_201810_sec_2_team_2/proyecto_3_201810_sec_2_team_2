package model.data_structures.digraph;

import java.io.Serializable;

/**
 * Interfaz utilizada para representar las responsabilidades m�nimas de un arco
 */
public interface IArco extends Serializable
{
    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Devuelve el peso del arco
     * @return Peso del arco
     */
    public double darPeso( );
}
