package model.data_structures.digraph;

import java.util.Iterator;

import model.data_structures.SeparateChainingHashTable;

/*  @author Robert Sedgewick
*  @author Kevin Wayne
*/

public class Kosaraju {
   private boolean[] marked;     // marked[v] = has vertex v been visited?
   private int[] id;             // id[v] = id of strong component containing v
   private SeparateChainingHashTable<String, Integer> hash;
   private int count;            // number of strongly-connected components

   /**
    * Computes the strong components of the digraph {@code G}.
    * @param G the digraph
 * @throws Exception 
    */
   public Kosaraju(GrafoDirigido G, GrafoDirigido GR) throws Exception {

       // compute reverse postorder of reverse graph
       GrafoDirigido gr = GR;

       // run DFS on G, using reverse postorder to guide calculation
       marked = new boolean[G.darVertices().size()];
       id = new int[G.darVertices().size()];
       System.out.println("g.darv: " + G.darVertices().size());
       hash = new SeparateChainingHashTable<>(G.darVertices().size());
       Iterator iter = gr.reversePost();
       
       while(iter.hasNext())
       {
    	   Vertice v = (Vertice) iter.next();
    	   if(!v.marcadoDFS())
    	   {
    		   dfs(G, v);
    		   count++;
    	   }
       }
       
       
       // check that id[] gives strong components
       assert check(G);
   }

   // DFS on graph G
   private void dfs(GrafoDirigido G, Vertice v) throws Exception { 
       v.marcarDFS();
//       id[v] = count;
       hash.put((String) v.darId(), count);
       for (int i = 0; i<G.darAdyacentes(v).size(); i++)
       {
    	   System.out.println("i: " + i);
    	   Vertice actual = (Vertice) G.darAdyacentes(v).getByPos(i);
    	   if(!actual.marcadoDFS())
    	   {
    		   dfs(G,v);
    	   }
       }
   }

   /**
    * Returns the number of strong components.
    * @return the number of strong components
    */
   public int count() {
       return count;
   }

   /**
    * Are vertices {@code v} and {@code w} in the same strong component?
    * @param  v one vertex
    * @param  w the other vertex
    * @return {@code true} if vertices {@code v} and {@code w} are in the same
    *         strong component, and {@code false} otherwise
 * @throws Exception 
    * @throws IllegalArgumentException unless {@code 0 <= v < V}
    * @throws IllegalArgumentException unless {@code 0 <= w < V}
    */
   public boolean stronglyConnected(Vertice v, Vertice w) throws Exception {
//       validateVertex(v);
//       validateVertex(w);
//       return id[v] == id[w];
	   return hash.get((String)v.darId()).equals(hash.get((String) w.darId()));
   }

   /**
    * Returns the component id of the strong component containing vertex {@code v}.
    * @param  v the vertex
    * @return the component id of the strong component containing vertex {@code v}
    * @throws IllegalArgumentException unless {@code 0 <= s < V}
    */
   public int id(int v) {
       validateVertex(v);
       return id[v];
   }

   // does the id[] array contain the strongly connected components?
   private boolean check(GrafoDirigido G) throws Exception {
       TransitiveClosure tc = new TransitiveClosure(G);
       for (int i = 0; i < G.darVertices().size(); i++) {
    	   Vertice v = (Vertice) G.darVertices().get(i);
           for (int j = 0; j < G.darVertices().size(); j++) {
        	   Vertice w = (Vertice) G.darVertices().get(j);
               if (stronglyConnected(v, w) != (tc.reachable(v, w) && tc.reachable(w, v)))
                   return false;
           }
       }
       return true;
   }

   // throw an IllegalArgumentException unless {@code 0 <= v < V}
   private void validateVertex(int v) {
       int V = marked.length;
       if (v < 0 || v >= V)
           throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
   }

   /**
    * Unit tests the {@code KosarajuSharirSCC} data type.
    *
    * @param args the command-line arguments
    */
   
}
