package model.data_structures.digraph;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import model.vo.VerticeServicios;

//AIzaSyA8LO3MQJhbzr2GXTGDuZFnrDhFYYxpt50
public class Editor
{
	public static void dibujoRequerimiento1(double lat, double lon){
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/mapa/tem.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "var myLatLng = {lat: "+lat+", lng: "+lon+"};" + 
					"var marker = new google.maps.Marker({" + 
					"    position: myLatLng," + 
					"    map: map," + 
					"    title: 'Vertice mas congestionado'" + 
					"  });"
			+" var cityCircle = new google.maps.Circle({"
			+"strokeColor: '#FF0000',"
			+"strokeOpacity: 0.8,"
			+"strokeWeight: 2,"
			+"fillColor: '#FF0000',"
			+"fillOpacity: 0.35,"
			+"map: map,"
			+"center: {lat:"+ lat +", lng:"+lon+"},"
			+"radius: Math.sqrt(0.2) * 100"
			+"});";
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/mapa/"+"mapa"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);		
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   }
	
	public static void dibujoRequerimiento3(String str){
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			
			String[] obj= str.split(";;;");
			//double tot=0;
			String vId="";
			String lat="";
			String lon="";
			double densidad=0;
			double radio=0;
			String scriptTag="";
			for(int i=0;i<obj.length;i++)
			{
				
					String[] subObj= obj[i].split(":::");
					for(int j=0;j<subObj.length;j++)
					{
						vId=subObj[0];
						
						String[]latlong= vId.split("/");
						
						lat= latlong[0];
						
						lon= latlong[1];
						
						densidad=Double.parseDouble(subObj[1]);
						radio=Double.parseDouble(subObj[2]);
						System.out.println((radio+densidad)*100);
						
					}
					 scriptTag += " var cityCircle"+i+ "= new google.maps.Circle({"
								+"strokeColor: '#FF0000',"
								+"strokeOpacity: 0.8,"
								+"strokeWeight: 2,"
								+"fillColor: '#FF0000',"
								+"fillOpacity: 0.35,"
								+"map: map,"
								+"center: {lat:"+ lat +", lng:"+lon+"},"
								+"radius: "+ (radio+densidad)*100
								+"});";

			}
		
			
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/mapa/"+"mapa3"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);		
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void dibujoRequerimiento4(List<String> ls)
	{
		System.out.println("Imprimio Mapa");
		try {
			File htmlTemplateFile = new File("data/template/template.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			
			String scriptTag = "var flightPlanCoordinates = [";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
				if(i!=ls.size()-1)
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"},";						
				}
				else
				{
					scriptTag+= "{lat: "+lat+","+ "lng: "+lon+"}";						

				}

			}
			scriptTag+="];"
					+ "var flightPath = new google.maps.Polyline({"
					+ "path: flightPlanCoordinates,"
					+ "geodesic: true,"
					+"strokeColor: '#FF0000',"
					+"strokeOpacity: 1.0,"
					+"strokeWeight: 2"
					+"});"
					+"flightPath.setMap(map);";
			for(int i=0; i< ls.size(); i++)
			{
				String[] valores=ls.get(i).split("/");
				String lat="";
				String lon="";
				lat= valores[0];
				lon=valores[1];
			
				scriptTag+= " var cityCircle"+i+ "= new google.maps.Circle({"
						+"strokeColor: '#FF0000',"
						+"strokeOpacity: 0.8,"
						+"strokeWeight: 2,"
						+"fillColor: '#FF0000',"
						+"fillOpacity: 0.35,"
						+"map: map,"
						+"center: {lat:"+ lat +", lng:"+lon+"},"
						+"radius: Math.sqrt(2) * 100"
						+"});";
			}
			htmlString = htmlString.replace("//$script", scriptTag);
			File newHtmlFile = new File("data/mapa/"+"mapa4"+".html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);	
			java.awt.Desktop.getDesktop().browse(newHtmlFile.toURI());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

}
