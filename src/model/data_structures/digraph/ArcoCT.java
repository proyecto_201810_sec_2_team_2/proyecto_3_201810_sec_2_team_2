package model.data_structures.digraph;


/**
 * Representa un arco en una clausura transitiva.
 */
public class ArcoCT implements IArco
{
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
	 * Constante para la serialización
	 */
	private static final long serialVersionUID = 1L;
	
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
    /**
     * Peso del arco.
     */
    private double peso;
    
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
    

    /**
     * Constructor por parámetros del arco.
     * @param peso Peso del arco.
     */
    public ArcoCT( double peso )
    {
        this.peso = peso;
    }

    // -----------------------------------------------------------------
    // Métodos
    // -----------------------------------------------------------------
    
    /**
     * Retorna el peso del arco.
     * @return El peso del arco.
     */
    public double darPeso( )
    {
        return peso;
    }

}
