package model.vo;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.google.gson.Gson;

import model.data_structures.digraph.ArcoNoExisteException;
import model.data_structures.digraph.ArcoYaExisteException;
import model.data_structures.digraph.GrafoDirigido;
import model.data_structures.digraph.GrafoDirigidoR;
import model.data_structures.digraph.Vertice;
import model.data_structures.digraph.VerticeNoExisteException;
import model.data_structures.digraph.VerticeYaExisteException;
import model.vo.VerticeServicios;


public class ControladorMundo2 {


	private double distanciaReferencia;
	private GrafoDirigido <String, VerticeServicios, ArcoServicios> grafoReverso;

	public ControladorMundo2(double distanciaReferencia)
	{
		this.distanciaReferencia=distanciaReferencia;
		grafoReverso=new GrafoDirigido<String, VerticeServicios, ArcoServicios>();
	}

	public void agregarServicio(String tripId, double seconds, double miles, double total,double pickUpLatitude,double pickUpLongitude,double dropOffLatitude,double dropOffLongitude, double peajes) throws VerticeNoExisteException, ArcoYaExisteException  
	{
		//si no tenemos la informacion de alguna de estas no tendriamos info suficiente para agregar los nodos
		try
		{
			VerticeServicios fin=agregarModificarVertice(tripId,pickUpLatitude,pickUpLongitude);
			System.out.println(fin.darId());
//			System.out.println(pickUpLatitude+pickUpLongitude);
			VerticeServicios inicio=agregarModificarVertice(tripId,dropOffLatitude,dropOffLongitude);
			inicio.agregarSu(tripId);
			fin.agregarPre(tripId);
			try
			{
				ArcoServicios arco=grafoReverso.darArco(inicio.darId(), fin.darId());
				arco.agregarValores(miles*1609.34, seconds, total, peajes);

			}
			catch (ArcoNoExisteException e)
			{
				grafoReverso.agregarArco(inicio.darId(), fin.darId(),new ArcoServicios(inicio.darId(), fin.darId(),miles*1609.34, seconds, total, peajes));
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void agregarArco(ArcoServicios arco)
	{

		try 
		{
			grafoReverso.agregarArco(arco.getIdVerticeDestino(), arco.getIdVerticeOrigen(), arco);

		} catch (VerticeNoExisteException | ArcoYaExisteException e) 
		{
			e.printStackTrace();
		}

	}
	public void agregarVertice(VerticeServicios vertice)
	{
		try
		{
			//			System.out.println("agregarVertice " + vertice.darId());
			grafoReverso.agregarVertice(vertice);
		} catch (VerticeYaExisteException e)
		{
			e.printStackTrace();
		}
	}

	public VerticeServicios agregarModificarVertice(String tripId,double latitude,double longitude) throws VerticeYaExisteException
	{
		VerticeServicios vertice=null;
		double distanciaMin=Double.POSITIVE_INFINITY;
		for(VerticeServicios actual:grafoReverso.darVertices())
		{
			double dist=distanciaEnMetros(actual.darLatitud(),actual.darLongitud(),latitude,longitude);
			if(dist<distanciaMin)
				vertice=actual;
		}
		if(vertice==null)
		{
			vertice=new VerticeServicios(latitude,longitude,tripId);
			grafoReverso.agregarVertice(vertice);
			return vertice;
		}
		vertice.agregarServicio(tripId);
		
		return vertice;

	}

	public double distanciaEnMetros(double pickupLatitud, double pickupLongitud,double pLat, double pLon) 
	{
		try
		{
			int r=6371*1000;
			Double latDist=toRad(pickupLatitud-pLat);
			Double lonDist=toRad(pickupLongitud-pLon);
			Double a = Math.sin(latDist/2)*Math.sin(latDist/2)
					+Math.cos(toRad(pLat))*Math.cos(toRad(pickupLatitud))
					*Math.sin(lonDist/2)*Math.sin(lonDist/2);
			Double c=2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			return distanciaReferencia>=(r*c)?(r*c):Double.POSITIVE_INFINITY;


		}
		catch(Exception e)
		{ 	
			e.printStackTrace();
			System.out.println("entro");
			return Double.POSITIVE_INFINITY;
		}
	}
	public double toRad(double grados)
	{
		return (grados/90)*3.1416;
	}

	public void imprimirInfo()
	{
		System.out.println("Informaci�n del grafo reverso: ");
		System.out.println("--N�mero de v�rtices: "+grafoReverso.darVertices().size());
		System.out.println("--N�mero de arcos: "+grafoReverso.darArcos().size());
		System.out.println("--Distancia utilizada: "+distanciaReferencia+" metros");
	}
	
	

	public String generarJSON(String tamanoDatos, String distanciaDeReferencia)
	{
		File archivoArcos = new File("./data/arcos-"+tamanoDatos+"-"+distanciaReferencia+"m.json");
		File archivoVertices = new File("./data/vertices-"+tamanoDatos+"-"+distanciaReferencia+"m.json");
		Gson gson=new Gson();


		try 
		{
			if(!archivoVertices.exists())
			{
				archivoVertices.createNewFile();

				PrintWriter pw = new PrintWriter(archivoVertices);
				pw.println(gson.toJson(grafoReverso.darVertices()));
				pw.close();


				if(!archivoArcos.exists())
					archivoArcos.createNewFile();

				PrintWriter pw1 = new PrintWriter(archivoArcos);
				pw1.println(gson.toJson(grafoReverso.darArcos()));
				pw1.close();
				return "El JSON se genero correctamente";
			}
			return "El JSON ya habia sido generado";

		}
		catch (FileNotFoundException e) {
			return "Hubo un problema escribiendo el JSON";
		} catch (IOException e) {
			return "Hubo un problema escribiendo el JSON";
		}
	}
	
	public GrafoDirigido darGrafo()
	{
		return grafoReverso;
	}
	
	
	
	public VerticeServicios darVerticeConMasCosos()
	{
		List<VerticeServicios>ls=grafoReverso.darVertices();
		int mayor=0;
		VerticeServicios vertice= null;
		for(VerticeServicios v:ls)
		{
			int count =v.darIdServicios().size();
			if(count>mayor)
			{
				mayor=count;
				vertice= v;
			}
		}
		return vertice; 
	}

	public VerticeServicios darVertice(double latitude,double longitude) throws Exception
	{
		VerticeServicios vertice=null;
		double distanciaMin=Double.POSITIVE_INFINITY;
		for(VerticeServicios actual:grafoReverso.darVertices())
		{
			double dist=distanciaEnMetros(actual.darLatitud(),actual.darLongitud(),latitude,longitude);
			if(dist<distanciaMin)
				vertice=actual;
		}
		if(vertice==null)
		{
			throw new Exception ("No existe un v�rtice cercano en las coordenadas dadas");
		}
		//		vertice.agregarServicio(tripId);
		return vertice;

	}

}