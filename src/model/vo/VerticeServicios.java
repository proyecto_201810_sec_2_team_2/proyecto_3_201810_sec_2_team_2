package model.vo;

import java.util.ArrayList;

import model.data_structures.digraph.IVertice;


public class VerticeServicios implements IVertice<String>,Comparable<VerticeServicios>
{
	private ArrayList<String> idServicios;
    private double latitud;
    private double longitud;
    private ArrayList sucesores;
    private ArrayList predecesores;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------    
    
    /**
     * Constructor de la clase.
     * @param valor Dato contenido por el v�rtice.
     */
    public VerticeServicios( double latitud,double longitud,String idServicio )
    {
        this.latitud = latitud;
        this.longitud = longitud;
        idServicios=new ArrayList<String>();
        sucesores = new ArrayList<>( );
         predecesores = new ArrayList<>( );
        idServicios.add(idServicio);
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------    
    public double darLatitud()
    {
    	return latitud;
    }
    public double darLongitud()
    {
    	return longitud;
    }
    public ArrayList<String> darIdServicios()
    {
    	return idServicios;
    }
    public void agregarServicio(String idServicio)
    {
    	idServicios.add(idServicio);
    }
    public void agregarPre(String idServicio)
    {
    	predecesores.add(idServicio);
    }
    public void agregarSu(String idServicio)
    {
    	sucesores.add(idServicio);
    }
    public int darNumPredecesores()
    {
    	return predecesores.size();
    }
    public int darNumSucesores()
    {
    	return sucesores.size();
    }
	@Override
	public String darId() {
		return latitud+"/"+longitud;
	}

	@Override
	public int compareTo(VerticeServicios arg0) {
		return darId().compareTo(arg0.darId());
	}
}